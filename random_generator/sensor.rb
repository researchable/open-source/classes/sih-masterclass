#!/usr/bin/ruby

require 'rdkafka'

ENV['CLOUDKARAFKA_TOPIC_PREFIX'] = '0u1ym17w-'
ENV['CLOUDKARAFKA_USERNAME'] = '0u1ym17w'
ENV['CLOUDKARAFKA_PASSWORD'] = '5zWP-EfokX5-o06bpNTvxVYKAqXcU-4Z'
ENV['CLOUDKARAFKA_URL'] = 'rocket-01.srvs.cloudkafka.com:9094'

cloud_hosted = true
config = if cloud_hosted
           {
             "bootstrap.servers": ENV['CLOUDKARAFKA_URL'],
             "sasl.username": ENV['CLOUDKARAFKA_USERNAME'],
             "sasl.password": ENV['CLOUDKARAFKA_PASSWORD'],
             "security.protocol": 'SASL_SSL',
             "sasl.mechanisms": 'SCRAM-SHA-256'
           }
         else
           {
             "bootstrap.servers": 'kafka:9092',
             "group.id": 'ruby-test'
           }
         end
topic = "#{ENV['CLOUDKARAFKA_TOPIC_PREFIX']}default"

rdkafka = Rdkafka::Config.new(config)
producer = rdkafka.producer

loop do
  sleep 1
  data = { value: rand }
  puts "Sending data: #{data}"
  producer.produce(
    topic: topic,
    payload: data.to_json
  ).wait
end
